# URL

https://api.store.nvidia.com/partner/v1/feinventory?skus=UK~NVGFT070~NVGFT080~NVGFT090~NVLKR30S~NSHRMT01~NVGFT060T~371&locale=UK


# URL Parameters
```
skus=UK~NVGFT070~NVGFT080~NVGFT090~NVLKR30S~NSHRMT01~NVGFT060T~371
locale=UK
```

# Request Headers

```
GET /partner/v1/feinventory?skus=UK~NVGFT070~NVGFT080~NVGFT090~NVLKR30S~NSHRMT01~NVGFT060T~371&locale=UK HTTP/1.1
Host: api.store.nvidia.com
User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0
Accept: application/json, text/plain, */*
Accept-Language: en-GB
Accept-Encoding: gzip, deflate, br
Origin: https://shop.nvidia.com
DNT: 1
Connection: keep-alive
Referer: https://shop.nvidia.com/
Sec-Fetch-Dest: empty
Sec-Fetch-Mode: cors
Sec-Fetch-Site: same-site
```

# Response Headers

```
HTTP/2 200 OK
content-type: application/json
x-amzn-requestid: 0602a022-d657-4000-ae5b-22fd59af188b
access-control-allow-origin: *
x-amz-apigw-id: HRXgVGSSoAMFuyQ=
x-amzn-trace-id: Root=1-616a0601-5f257df60906fd5513c1000d;Sampled=0
x-amz-cf-pop: MXP64-C1
x-amz-cf-id: zkhdkEPy5uzPF6ttYXpwK3djWOcud0fVspozi5BKfHYQqJsR2rC5XQ==
x-edgeconnect-midmile-rtt: 24
x-edgeconnect-origin-mex-latency: 131
vary: Accept-Encoding
content-encoding: gzip
date: Fri, 15 Oct 2021 22:51:46 GMT
content-length: 280
set-cookie: ak_bmsc=C00780C54C714FC332C8FA0752E172A6~000000000000000000000000000000~YAAQDkYUAvOGL1h8AQAA1ncnhg3FnF7BENQBA/Q1lrRTPXpMUyl0icuFOOtWMlYvGCGd06h6LmaM2zRZT7sx7RIZN4RlbUzTw0XX4MPkUiNH3P3DO8H6xU9PjLoElOMR41Q+lcAj7CsHI4kLuuuqsrl1kqekD4nnTFMSJa0FzIOv+qc5tmPWD92ahXyZYT+pZO7yVoEdB5uLM4CvSNu1sgZ3xJJa4SMBYavbm8hbauSUGJaDi3Y8j/wMV9LTj/aeS53ZZABeBOPvRmz3sMz0E909mJlpB9JEQT7KLD1N3sWodSAM9TFooT3KSZ0P/uULkG8g/LSaPFprcpa4ZVEgXYht7UAVdQp6xigQu/JKm+KGx5WmYWUnyvkvSLmkIoQv9OZcUegDYjVNhY94dAcmbA==; Domain=.store.nvidia.com; Path=/; Expires=Sat, 16 Oct 2021 00:51:45 GMT; Max-Age=7199; HttpOnly
X-Firefox-Spdy: h2
```