# URL

https://api.nvidia.partners/edge/product/search?page=1&limit=9&locale=en-gb&category=GPU&gpu=RTX%203080,RTX%203060,RTX%203060%20Ti,RTX%203070,RTX%203070%20Ti,RTX%203080%20Ti,RTX%203090&manufacturer=NVIDIA&gpu_filter=RTX%203090~1,RTX%203080%20Ti~1,RTX%203080~1,RTX%203070%20Ti~1,RTX%203070~1,RTX%203060%20Ti~1,RTX%203060~0,RTX%203050%20Ti~0,RTX%203050~0,RTX%202080%20Ti~0,RTX%202080%20SUPER~0,RTX%202080~0,RTX%202070%20SUPER~0,RTX%202070~0,RTX%202060%20SUPER~0,RTX%202060~0,GTX%201660%20Ti~0,GTX%201660%20SUPER~0,GTX%201660~0,GTX%201650%20Ti~0,GTX%201650%20SUPER~0,GTX%201650~0

# URL Parameters

```
page=1
limit=9
locale=en-gb
category=GPU
gpu=RTX%203080,RTX%203060,RTX%203060%20Ti,RTX%203070,RTX%203070%20Ti,RTX%203080%20Ti,RTX%203090
manufacturer=NVIDIA
gpu_filter=RTX%203090~1,RTX%203080%20Ti~1,RTX%203080~1,RTX%203070%20Ti~1,RTX%203070~1,RTX%203060%20Ti~1,RTX%203060~0,RTX%203050%20Ti~0,RTX%203050~0,RTX%202080%20Ti~0,RTX%202080%20SUPER~0,RTX%202080~0,RTX%202070%20SUPER~0,RTX%202070~0,RTX%202060%20SUPER~0,RTX%202060~0,GTX%201660%20Ti~0,GTX%201660%20SUPER~0,GTX%201660~0,GTX%201650%20Ti~0,GTX%201650%20SUPER~0,GTX%201650~0
```

# Request Headers

```
GET /edge/product/search?page=1&limit=9&locale=en-gb&category=GPU&gpu=RTX%203080,RTX%203060,RTX%203060%20Ti,RTX%203070,RTX%203070%20Ti,RTX%203080%20Ti,RTX%203090&manufacturer=NVIDIA&gpu_filter=RTX%203090~1,RTX%203080%20Ti~1,RTX%203080~1,RTX%203070%20Ti~1,RTX%203070~1,RTX%203060%20Ti~1,RTX%203060~0,RTX%203050%20Ti~0,RTX%203050~0,RTX%202080%20Ti~0,RTX%202080%20SUPER~0,RTX%202080~0,RTX%202070%20SUPER~0,RTX%202070~0,RTX%202060%20SUPER~0,RTX%202060~0,GTX%201660%20Ti~0,GTX%201660%20SUPER~0,GTX%201660~0,GTX%201650%20Ti~0,GTX%201650%20SUPER~0,GTX%201650~0 HTTP/1.1
Host: api.nvidia.partners
User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0
Accept: application/json, text/plain, */*
Accept-Language: en-GB
Accept-Encoding: gzip, deflate, br
Origin: https://shop.nvidia.com
DNT: 1
Connection: keep-alive
Referer: https://shop.nvidia.com/
Sec-Fetch-Dest: empty
Sec-Fetch-Mode: cors
Sec-Fetch-Site: cross-site
```

# Response Headers

```
HTTP/2 200 OK
content-type: application/json
x-amzn-requestid: ec93e2f2-2b26-4764-b09c-20ebad2cd6dd
access-control-allow-origin: *
x-amz-apigw-id: HRXgZEw5IAMFqpw=
x-amzn-trace-id: Root=1-616a0602-3ec34f744110da7409d0d3e3
x-amz-cf-pop: MXP63-P1
x-amz-cf-id: hUZ91sZCTckfTIwDr4N8CO-5XXAt6Fbd60OD3KdrAXttAnqs1gdFlg==
content-encoding: gzip
content-length: 1918
x-edgeconnect-midmile-rtt: 20, 25
x-edgeconnect-origin-mex-latency: 805, 805
cache-control: max-age=1800
expires: Fri, 15 Oct 2021 23:21:46 GMT
date: Fri, 15 Oct 2021 22:51:46 GMT
vary: Accept-Encoding
set-cookie: ak_bmsc=7BDAA599237AB373E4428C8AED5C7977~000000000000000000000000000000~YAAQFkYUApWIIlR8AQAAS3snhg37x/oOrh6/7laRp7wLVwvTWOv4auaoC9ZGDdetyr8j2Yd3Dp7yugTFl683DR7zgT2q+2gbWLCwMjHki/Sq5RV4iJh8QQyoUoZ1CUc+ZI9In3n71umXUD5uSbULPlTBfvqln5StQbV0TFPDJO8lR05ppSOAyII3LmhaQD+N04S9cXASPoJA0M1sx9ruT23I1uqjYesX/djLL/j9rWveehkFtzArwPXu935fIEbKXYu4EIBfjpRQcYy3kaTJg6fRV3klIB9M3BPXUh7PbbkOWRhG5X1M6l+vV8fsCAONOxxbkAPcpXKwCRhq25bcSk55j36f23YQ45xqyjYMLYsPeldHx9DuE/OsE/44yDd0+CUVg8o/d2jEWAKWb+8L; Domain=.nvidia.partners; Path=/; Expires=Sat, 16 Oct 2021 00:51:46 GMT; Max-Age=7200; HttpOnly
X-Firefox-Spdy: h2
```