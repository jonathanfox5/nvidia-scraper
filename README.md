# nvidia-scraper

## Purpose

Tool to scrape nvidia site for founders edition cards coming into stock and notify user by SMS.

It configured by default to check for UK founders edition cards but it can be customised for other locales or to use the main search page API for AIB cards instead. See "Configuration and troubleshooting".

## Installation

These steps assume installation on linux and have been tested on Fedora 34 and Ubuntu 20 LTS (server). Although it has not (yet) been tested on Raspberry Pi, it's probably a good use for one if you have one lying around!

The code should also run fine on Windows / macOS but the installation steps will vary from those outlined below.

1. Git clone this repository

```bash
git clone https://gitlab.com/jonathanfox5/nvidia-scraper
```

2. Enter the created folder, create a virtual environment, install required packages and enter the virtual environment for testing purposes.

```bash
cd nvidia-scraper
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

3. Copy `tool/samepledotenv.env` to `tool/.env`. Populate with the details of your twilio.com account (the free version works fine for SMS alerts to a single phone number). Note that`vim` below can be replaced with `nano` for a more user friendly experience.

```bash
cp tool/sampledotenv.env tool/.env
vim tool/.env
```

4. Your configuration can be tested by running main.py in the tool folder.

```bash
python tool/main.py
```

5. You should receive an SMS for each card on the website when the tool starts. The true / false in the SMS denotes whether the card is in stock or not. SMS alerts will be sent when any details change or if there is an error. You can also inspect `tool/nvs_events.log` to check that everything is working as expected.

6. The tool can be stopped with ctrl-c

7. To have the scraper run in the background when the server starts, create a service. Copy the service config from the config folder, update the paths and username to match your installation and use systemctl to start / enable it. Note that`vim` below can be replaced with `nano` for a more user friendly experience.

```bash
sudo cp config/nvidia-scraper.service /etc/systemd/system/nvidia-scraper.service
sudo vim /etc/systemd/system/nvidia-scraper.service
sudo systemctl start nvidia-scraper
sudo systemctl enable nvidia-scraper
```

## Configuration and troubleshooting

The geographical store can be changed in `tool/main.py` by updating the global variable `LOCALE`. UK, DE, ES, NL and FR have been tested.

The refresh rate of the tool (i.e. how often it checks the site for changes) is set by the global variable `REFRESH_TIME` in `tool/main.py`.

To avoid the tool spamming the user with errors, the tool will not send another non-critical error message once one has been sent for a period of time. This is controlled by `ERROR_NOTIFICATION_SPACING` in `tool/main.py`.

Non-critical errors should not affect stability and are defined as those that have been handled within the program e.g. no data from nvidia server, response is deformed, etc. The error SMS will read "No data available! nvidia-scraper running but not picking up data".

Critical errors are dealt with by a generic error handler and halt the program. These will likely be due to e.g. response format changing. The program is therefore stopped to avoid nonsense outputs. The error SMS will read "Unhandled exception! nvidia-scraper stopped".

Note that the nvidia api is very sensitive to the headers used. It will either slow requests to 10 seconds or provide an error if it doesn't like them. `curl` is useful for debugging and there are curl examples are in the `apis` folder. If the api is updated, you may need to get a new url / parameters / headers from the nvidia site. In order to work these out, use the "network" section of the built in "inspect" tool within your web browser to monitor the requests that your browser makes on the nvidia stock page.

Information on both the nvidia fe inventory api and the AIB search api is available in the `apis` folder should you wish to make changes. `request.md` details the requirements and `curl.txt` has a sample curl command that you can use to test should you have any issues with the python tool. Sample output is in `response.notformatted.json`.

Good luck!
