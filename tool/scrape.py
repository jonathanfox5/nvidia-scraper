import requests

import logging
log = logging.getLogger(__name__)

DEFAULT_HEADERS = {
    'User-Agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0',
    'Accept': 'application/json, text/plain, */*',
    'Accept-Language': 'en-GB',
    'Origin': 'https://shop.nvidia.com',
    'DNT': '1',
    'Connection': 'keep-alive',
    'Referer': 'https://shop.nvidia.com/',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-site',
    'TE': 'trailers',
}


def get_data(url, query, headers, timeout=15):

    try:
        # Grab Data
        response = requests.get(
            url=url, params=query, headers=headers, timeout=timeout)

        # Raise error if invalid response received
        response.raise_for_status()

        # Test that we have a valid json. If this fails, this will throw an error that we can catch
        j = response.json()

        # Return as a text object so we can easily do a comparison
        t = response.text
        return t

    except Exception as e:
        logging.error(e)
        return None


def get_fe_inventory(locale="UK"):
    url = "https://api.store.nvidia.com/partner/v1/feinventory"

    query = {"skus": "NVGFT070~NVGFT080~NVGFT090~NVGFT060T~NVGFT070T~NVGFT080T",
             "locale": locale}

    headers = DEFAULT_HEADERS

    d = get_data(url, query, headers)

    # Return error if there was an error
    if d is None:
        d = "error"

    logging.debug(d)

    return d
