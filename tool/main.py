import time
import json
import logging
import os

import notify_sms
import scrape

# Configure logging to file. Save log to same directory as main.py
log_dir = os.path.abspath(os.path.dirname(__file__))
log_file = (os.path.join(log_dir, "nvs_events.log"))

log = logging.basicConfig(filename=log_file, level=logging.INFO,
                          format='%(asctime)s -  %(levelname)s -  %(message)s')

# Global variables for configuration
REFRESH_TIME = 2.0  # In seconds
ERROR_NOTIFICATION_SPACING = 28800.0  # In seconds. 28800s = 8 hours
LOCALE = "UK"  # Change store location to UK, DE, ES, NL or FR as required.


def main():

    # Initialise
    sms = notify_sms.SMS()
    previous_time = 0.0
    previous_data = "first run"
    error_notification_valid_after = time.time()
    logging.info("Scraper started")

    # Main loop
    try:
        while (True):

            # Limit the loop to a pre-defined refresh time
            elapsed_time = time.time() - previous_time
            if (elapsed_time < REFRESH_TIME):
                time.sleep(REFRESH_TIME - elapsed_time)

            previous_time = time.time()

            # Get data
            new_data = scrape.get_fe_inventory(LOCALE)

            # If there is a data error, log it and send a message if enough time has passed since the previous error notification
            if new_data == "error":
                logging.error("Error in retrieving data from server")

                if time.time() > error_notification_valid_after:
                    logging.info("Data error notification sent")
                    sms.send_message(
                        "No data available! nvidia-scraper running but not picking up data")

                    error_notification_valid_after = time.time() + ERROR_NOTIFICATION_SPACING

            # Otherwise, if we previously had an error - log that we are back in business
            elif previous_data == "error":
                logging.info("Data retrieval back online - back in business!")

            # Process data if it has changed
            if (new_data != previous_data):

                compare_data(new_data, previous_data, sms)

                # Set up for next loop
                previous_data = new_data
    except:
        # Send SMS that something broke
        sms.send_message("Unhandled exception! nvidia-scraper stopped")


def compare_data(new_data, old_data, sms):

    # If data is blank because there was an error, ignore it
    if (old_data == "error" or new_data == "error"):
        return

    # Pull card data from json and convert into dictionary.
    # old_data will be blank on first run so don't try to convert it
    new_cards = extract_card_data(new_data)
    if (old_data != "first run"):
        old_cards = extract_card_data(old_data)

    # Run through all cards in "new_card" dictionary
    for card in new_cards.keys():

        # If this is the first run, send them all to make debugging easier
        if old_data == "first run":
            logging.info("First run: " + card)
            sms.send_card(card, new_cards[card])

        # Otherwise, if card already exists, send sms if there is a difference
        elif card in old_cards:
            if ((new_cards[card]["active"] != old_cards[card]["active"]) or (new_cards[card]["url"] != old_cards[card]["url"]) or (new_cards[card]["price"] != old_cards[card]["price"])):
                logging.info("Change detected: " + card)
                sms.send_card(card, new_cards[card])

        # Otherwise, this is a new card. Send sms.
        # This should never be reached in normal operation
        else:
            logging.info("New card detected: " + card)
            sms.send_card(card, new_cards[card])


def extract_card_data(card_data):
    # Pull card data from json and convert into dictionary
    j = json.loads(card_data)["listMap"]

    card_dict = {}
    for card in j:
        card_data = {"active": card["is_active"],
                     "url": card["product_url"], "price": card["price"]}
        card_dict.setdefault(card["fe_sku"], card_data)

    return card_dict


def print_json(j):
    # Utility to help with debugging
    print(json.dumps(j, indent=2))


if __name__ == "__main__":
    main()
