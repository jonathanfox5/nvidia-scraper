import os
from twilio.rest import Client
from dotenv import load_dotenv

import logging
log = logging.getLogger(__name__)


class SMS():

    SID = ""
    TOKEN = ""
    PHONE_FROM = ""
    PHONE_TO = ""
    SMS_ON = ""

    twilio_client = None

    def __init__(self):

        # Load from dot env file
        basedir = os.path.abspath(os.path.dirname(__file__))
        load_dotenv(os.path.join(basedir, ".env"))

        self.SID = os.getenv("TWILIO_SID")
        self.TOKEN = os.getenv("TWILIO_TOKEN")
        self.PHONE_FROM = os.getenv("TWILIO_PHONE_FROM")
        self.PHONE_TO = os.getenv("TWILIO_PHONE_TO")
        self.SMS_ON = os.getenv("SMS_ON")

        self.twilio_client = Client(self.SID, self.TOKEN)

    def send_card(self, card, card_details):
        # Build up SMS message with status of a card

        msg_body = card + " / " + \
            card_details["active"] + " / " + \
            card_details["price"] + " / " + card_details["url"]

        self.send_message(msg_body)

    def send_message(self, msg_body):
        # Send SMS

        if self.SMS_ON == "yes":
            self.twilio_client.messages.create(
                body=msg_body, from_=self.PHONE_FROM, to=self.PHONE_TO)

        logging.info("SMS sent: " + msg_body)
